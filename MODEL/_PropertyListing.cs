﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MODEL
{
    public class _PropertyListing
    {
        public _PropertyListing()
        {
        }

        public _PropertyListing(object property)
        {

        }

        public string ID { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public Gender Gender { get; set; }
        public bool RoommateIsEmployed { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ApartmentNumber { get; set; }
        public string ZipCode { get; set; }
        public string PropertyDescription { get; set; }
        public string[] PropertyImages { get; set; }
        public bool ShortTermListing { get; set; }
        public DateTime MoveInDate { get; set; }
        public int DurationInWeeks { get; set; }
        public decimal YearlyRent { get; set; }
        public decimal Deposit { get; set; }

        public _Amenities Amenities { get; set; }
    }
}