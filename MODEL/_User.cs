﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODEL
{
    public class _User
    {
        public _User()
        {
        }

        public _User (object applicationUser)
        {
            ID = ((dynamic)applicationUser).Id;
            Name = ((dynamic)applicationUser).Name;
            Email = ((dynamic)applicationUser).Email;
            Phone = ((dynamic)applicationUser).Phone;

            Employed = ((dynamic)applicationUser).Employed;
            Occupation = ((dynamic)applicationUser).Occupation;
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone  { get; set; }

        public bool Employed { get; set; }
        public string Occupation { get; set; }
        public string ShortDescription { get; set; }

        public string ProfileImage { get; set; }
    }
}
