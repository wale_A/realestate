﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ViewModels
{
    public class EditUserViewModel
    {
        public string ShortDescription { get; set; }
        public bool Employed { get; set; }
        public string Occupation { get; set; }
    }
}