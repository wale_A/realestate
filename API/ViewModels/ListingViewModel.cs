﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.ViewModels
{
    public class ListingViewModel
    {
        public int RoommateMinAge { get; set; }
        public int RoommateMaxAge { get; set; }
        public Models.Gender RoommateGender { get; set; }
        public bool RoommateIsEmployed { get; set; }
        //public bool CompleteBackgroundCheck { get; set; }


        //property
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ApartmentNumber { get; set; }
        public string ZipCode { get; set; }
        public string PropertyDescription { get; set; }

        //listing
        public bool ShortTermListing { get; set; }

        //short term listing
        public DateTime MoveInDate { get; set; }
        public int DurationInWeeks { get; set; }

        //long term listing
        public decimal YearlyRent { get; set; }
        public decimal Deposit { get; set; }
    }
}