﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using API.ViewModels;
using Microsoft.AspNet.Identity;

namespace API.Controllers
{
    public class UsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Users
        public IQueryable<ApplicationUser> GetApplicationUsers()
        {
            return db.ApplicationUsers;
        }

        // GET: api/Users/5
        [ResponseType(typeof(ApplicationUser))]
        public async Task<IHttpActionResult> GetApplicationUser(string id)
        {
            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return Ok(new MODEL._User(applicationUser));
        }

        // PUT: api/Users/5
        //[ResponseType(typeof(void))]
        [Authorize]
        public async Task<HttpResponseMessage> Put([FromBodyAttribute] EditUserViewModel model)
        {
            try
            {
                var user = await db.ApplicationUsers.FindAsync(User.Identity.GetUserId());
                if (user == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "");
                }
                else
                {
                    user.ShortDescription = model.ShortDescription;
                    user.Employed = model.Employed;
                    user.Occupation = model.Occupation;

                    db.Entry(user).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return Request.CreateResponse(HttpStatusCode.OK, new MODEL._User(user));
                }
            }
            catch (Exception ex)
            {
                //logg error
                // roll back with EF
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        //// POST: api/Users
        //[ResponseType(typeof(ApplicationUser))]
        //public async Task<IHttpActionResult> PostApplicationUser(MODEL._User model)
        //{
        //    try
        //    {
        //        var applicationUser = await db.ApplicationUsers.FindAsync(User.Identity.GetUserId());


        //        db.ApplicationUsers.Add(applicationUser);

        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (ApplicationUserExists(applicationUser.Id))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtRoute("DefaultApi", new { id = applicationUser.Id }, applicationUser);
        //}



        [Authorize]
        public async Task<IHttpActionResult> DeleteApplicationUser()
        {
            ApplicationUser applicationUser = await db.ApplicationUsers.FindAsync(User.Identity.GetUserId());
            if (applicationUser == null)
            {
                return NotFound();
            }

            db.ApplicationUsers.Remove(applicationUser);
            await db.SaveChangesAsync();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();   
            }
            base.Dispose(disposing);
        }

        private bool ApplicationUserExists(string id)
        {
            return db.ApplicationUsers.Count(e => e.Id == id) > 0;
        }
    }
}