﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using PagedList;
using API.ViewModels;

namespace API.Controllers
{
    public class ListingsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Listings
        public IHttpActionResult GetPropertyListings(int pageNumber = 1, int pageSize = 10, ListingViewModel searchParameters = null)
        {
            var propertyListings = new List<MODEL._PropertyListing>();
            if (searchParameters == null)
            {
                var listings = db.PropertyListings.ToPagedList(pageNumber, pageSize);
                propertyListings = listings.Select(x => new MODEL._PropertyListing(x)).ToList();
            }
            return Ok(propertyListings);
        }

        // GET: api/Listings/5
        [ResponseType(typeof(PropertyListing))]
        public async Task<IHttpActionResult> GetPropertyListing(string id)
        {
            PropertyListing propertyListing = await db.PropertyListings.FindAsync(id);
            if (propertyListing == null)
            {
                return NotFound();
            }
            return Ok(new MODEL._PropertyListing(propertyListing));
        }

        // PUT: api/Listings/5
        [Authorize]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPropertyListing(string id, PropertyListing propertyListing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != propertyListing.ID)
            {
                return BadRequest();
            }

            db.Entry(propertyListing).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PropertyListingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Listings
        [ResponseType(typeof(PropertyListing))]
        public async Task<HttpResponseMessage> PostPropertyListing(ListingViewModel model)
        {
            try
            {
                PropertyListing propertyListing = new PropertyListing()
                {
                    Address = model.Address,
                    ApartmentNumber = model.ApartmentNumber,
                    City = model.City,
                    Deposit = model.Deposit,
                    DurationInWeeks = model.DurationInWeeks,
                    MoveInDate = model.MoveInDate,
                    PropertyDescription = model.PropertyDescription,
                    RoommateGender = model.RoommateGender,
                    RoommateIsEmployed = model.RoommateIsEmployed,
                    RoommateMaxAge = model.RoommateMaxAge,
                    RoommateMinAge = model.RoommateMinAge,
                    ShortTermListing = model.ShortTermListing,
                    State = model.State,
                    YearlyRent = model.YearlyRent,
                    ZipCode = model.ZipCode
                };
                db.PropertyListings.Add(propertyListing);

                //if (System.Web.HttpContext.Current.Request.Files.Count > 0)
                //{
                //    foreach (var file in System.Web.HttpContext.Current.Request.Files)
                //    {

                //    }
                //}
                //else
                //{
                //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "no images attached");
                //}

                try
                {
                    await db.SaveChangesAsync();
                    return Request.CreateResponse(HttpStatusCode.Created, propertyListing);
                }
                catch (DbUpdateException ex)
                {
                    if (PropertyListingExists(propertyListing.ID))
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.Conflict, ex.Message);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message)
            }
        }

        // DELETE: api/Listings/5
        [ResponseType(typeof(PropertyListing))]
        public async Task<IHttpActionResult> DeletePropertyListing(string id)
        {
            PropertyListing propertyListing = await db.PropertyListings.FindAsync(id);
            if (propertyListing == null)
            {
                return NotFound();
            }

            db.PropertyListings.Remove(propertyListing);
            await db.SaveChangesAsync();

            return Ok(propertyListing);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PropertyListingExists(string id)
        {
            return db.PropertyListings.Count(e => e.ID == id) > 0;
        }
    }
}