﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Threading;
using System.Linq;
using System.Data.Entity;

namespace API.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public string ShortDescription { get; set; }
        public string ProfileImage { get; set; }
        public string Name { get; set; }

        public bool Employed { get; set; }
        public string Occupation { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public override int SaveChanges()
        {
            //return base.SaveChanges();
            throw new InvalidOperationException("username must be provided");
        }

        public override Task<int> SaveChangesAsync()
        {
            //return base.SaveChangesAsync();
            throw new InvalidOperationException("username must be provided");
        }

        public int SaveChanges(string username)
        {

            modifyEntities(username);
            return base.SaveChanges();
        }

        public async Task<int> SaveChangesAsync(string username)
        {
            // adding new entities
            modifyEntities(username);
            return await this.SaveChangesAsync(CancellationToken.None);
        }

        private void modifyEntities(string username)
        {
            var entitiesToBeAdded = ChangeTracker.Entries().Where(x => x.Entity is BaseClass && x.State == EntityState.Added).ToList();
            var entitiesToBeUpdated = ChangeTracker.Entries().Where(x => x.Entity is BaseClass && x.State == EntityState.Modified).ToList();
            var entitiesToBeDeleted = ChangeTracker.Entries().Where(x => x.Entity is BaseClass && x.State == EntityState.Deleted).ToList();

            foreach (var entity in entitiesToBeAdded)
            {
                ((BaseClass)entity.Entity).ID = Guid.NewGuid().ToString("N");
                ((BaseClass)entity.Entity).CreatedDateTime = DateTime.UtcNow.AddHours(1);
                ((BaseClass)entity.Entity).CreatedBy = username;
            }

            foreach (var entity in entitiesToBeUpdated)
            {
                ((BaseClass)entity.Entity).ModifiedDateTime = DateTime.UtcNow.AddHours(1);
                ((BaseClass)entity.Entity).ModifiedBy = username;
            }

            foreach (var entity in entitiesToBeDeleted)
            {
                ((BaseClass)entity.Entity).Deleted = (new Random()).Next(1, 10);
                ((BaseClass)entity.Entity).ModifiedDateTime = DateTime.UtcNow.AddHours(1);
                ((BaseClass)entity.Entity).ModifiedBy = username;
            }
        }

        public System.Data.Entity.DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public System.Data.Entity.DbSet<PropertyListing> PropertyListings { get; set; }
    }
}